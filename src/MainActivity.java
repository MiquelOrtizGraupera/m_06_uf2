

import java.sql.*;

public class MainActivity {
    public static void main(String[] args) throws SQLException {
       Ex1_MostrarAssignaturas();
        System.out.println();
       Ex2_MostrarDatosAlumnos();
        System.out.println();
       Ex3_MostrarNotas();
        System.out.println();
        Ex4_mostrarNotesAlumne();
        System.out.println();
       Ex5_Insert3Alumnes();
        System.out.println();
        Ex6_InsertNotas();
        System.out.println();
       Ex_7UpdateAlumnoNotas();
        System.out.println();
        Ex_8UpdatePhone();
        System.out.println();
        Ex9_EliminarAlumne();
    }

private static void Ex1_MostrarAssignaturas(){
    try {

        Connection conexion = DriverManager.getConnection("jdbc:postgresql://192.168.56.101:5432/school", "school", "school");
        String sql = "SELECT * FROM ASIGNATURAS";
        Statement sentencia = conexion.createStatement();
        boolean valor = sentencia.execute(sql);

        if (valor) {
            ResultSet rs = sentencia.getResultSet();
            while (rs.next())
                System.out.printf("COD: %d, %s, %n",
                        rs.getInt(1), rs.getString(2));
            rs.close();
        } else {
            int f = sentencia.getUpdateCount();
            System.out.printf("Filas afectadas:%d %n", f);
        }

        sentencia.close();
        conexion.close();
    } catch (SQLException e) {
        System.out.println("Connection failure.");
        e.printStackTrace();
    }
}

private static void Ex2_MostrarDatosAlumnos(){
        try{
            Connection conexion = DriverManager.getConnection("jdbc:postgresql://192.168.56.101:5432/school","school","school");
            String sql = "SELECT * FROM alumnos";
            Statement sentencia = conexion.createStatement();
            boolean valor = sentencia.execute(sql);

            if(valor){
                ResultSet resultat = sentencia.getResultSet();
                while(resultat.next()){
                    System.out.printf("FILA: %d, %s, %s, %s, %d, %n",
                            resultat.getInt(1), resultat.getString(2),
                    resultat.getString(3),resultat.getString(4),resultat.getInt(5));
                }
                resultat.close();
            }else {
                int f = sentencia.getUpdateCount();
                System.out.printf("Filas afectadas:%d %n", f);
            }
            sentencia.close();
            conexion.close();
        }catch (SQLException  e){
            System.out.println("No s'sha conectat a la BBDD");
            e.printStackTrace();
        }
}

private static void Ex3_MostrarNotas(){

        try{
            Connection conexion = DriverManager.getConnection("jdbc:postgresql://192.168.56.101:5432/school","school","school");
            String sql = "SELECT * FROM notas";
            Statement sentencia = conexion.createStatement();
            boolean valor = sentencia.execute(sql);

            if(valor){
                ResultSet resultat = sentencia.getResultSet();
                while (resultat.next()){
                    System.out.printf("FILA: %d, %d, %d, %n",
                            resultat.getInt(1),resultat.getInt(2),resultat.getInt(3));
                }
                resultat.close();
            }
            sentencia.close();
            conexion.close();
        }catch (SQLException e){
            System.err.println("NO s'ha conectat a la BBDD");
            e.printStackTrace();
        }
    }

    private static void Ex4_mostrarNotesAlumne(){
        try{
            Connection conexion = DriverManager.getConnection("jdbc:postgresql://192.168.56.101:5432/school","school","school");
            String sql = "SELECT nota FROM notas WHERE dni like '4448242'";
            Statement sentencia = conexion.createStatement();
            boolean valor = sentencia.execute(sql);

            if(valor){
                ResultSet resultat = sentencia.getResultSet();
                while(resultat.next()){
                    System.out.printf("DNI: 448242 --> %d %n ",
                            resultat.getInt(1));
                }
                resultat.close();
            }
            sentencia.close();
            conexion.close();
        }catch (SQLException e){
            System.err.println("Error de conexió a BBDD");
            e.printStackTrace();
        }
    }

    private static void Ex5_Insert3Alumnes(){
        try{
            Connection conexio = DriverManager.getConnection("jdbc:postgresql://192.168.56.101:5432/school","school","school");
            Statement sentencia = conexio.createStatement();
            String sql = "INSERT INTO ALUMNOS VALUES ('123254','Kintaso, kiku','Av.Catalunya 2','Mataro','93785421')";
            String sql1 = "INSERT  INTO ALUMNOS VALUES ('89754','Torres, Fer','Av.Molera 6','Granollers','93786541')";
            String sql2 = "INSERT  INTO ALUMNOS VALUES ('3874351','Hamsa, Abraham','C/Closens 4','Caldetes','65476121')";
            sentencia.executeUpdate(sql);
            sentencia.executeUpdate(sql1);
            sentencia.executeUpdate(sql2);
            sentencia.close();
            conexio.close();
            System.out.println("INSERTS INTRODUITS AMB EXIT");
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    /**
     * Només introdueix el segon UPDATE.
     * @throws SQLException
     */
    private static void Ex6_InsertNotas() throws SQLException {
    Connection conexio = DriverManager.getConnection("jdbc:postgresql://192.168.56.101:5432/school","school","school");
    PreparedStatement psInsertar = conexio.prepareStatement("INSERT INTO NOTAS VALUES(?,?,?)");
/**
 * Primer alumne 1-UPDATE
 */
        try {
            psInsertar.setInt(1,123254);
            psInsertar.setInt(2,4);
            psInsertar.setInt(3,8);

            psInsertar.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
/**
 * Primer alumne 2-Update
  */
        try{
            psInsertar.setInt(1, 123254);
            psInsertar.setInt(2, 5);
            psInsertar.setInt(3, 8);

            psInsertar.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
/**
 * Segon Alumne 1-Update
 */
        try {
            psInsertar.setInt(1,89754);
            psInsertar.setInt(2,4);
            psInsertar.setInt(3,8);

            psInsertar.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
/**
 * Segon alumne 2-Update
  */
    try{
        psInsertar.setInt(1,89754);
        psInsertar.setInt(2, 5);
        psInsertar.setInt(3, 8);
    }catch (SQLException e){
        e.printStackTrace();
    }
/**
 * Tercer Alumne 1-update
  */
        try {
            psInsertar.setInt(1,3874351);
            psInsertar.setInt(2,4);
            psInsertar.setInt(3,8);

            psInsertar.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
/**
 * Tercer alumne 2-update
  */
        try{
            psInsertar.setInt(1,3874351);
            psInsertar.setInt(2, 5);
            psInsertar.setInt(3, 8);
        }catch (SQLException e){
            e.printStackTrace();
        }

        psInsertar.close();
        conexio.close();

        System.out.println("Notes introduides amb exit");
    }

    private static void Ex_7UpdateAlumnoNotas() throws SQLException {
        Connection conexio = DriverManager.getConnection("jdbc:postgresql://192.168.56.101:5432/school","school","school");
        String sql = "UPDATE NOTAS SET NOTA = 9 WHERE DNI LIKE '4448242'";
        Statement sentencia = conexio.createStatement();
        sentencia.executeUpdate(sql);
        sentencia.close();
        conexio.close();
    }

    private static void Ex_8UpdatePhone() throws SQLException {
        Connection conexio = DriverManager.getConnection("jdbc:postgresql://192.168.56.101:5432/school","school","school");
        String sql = "UPDATE ALUMNOS SET telef = 934885237 WHERE DNI LIKE '12344345'";
        Statement sentencia = conexio.createStatement();
        sentencia.executeUpdate(sql);
        sentencia.close();
        conexio.close();
    }

    private static void Ex9_EliminarAlumne() throws SQLException {
        Connection conexio = DriverManager.getConnection("jdbc:postgresql://192.168.56.101:5432/school","school","school");
        String sql = "DELETE FROM ALUMNOS WHERE pobla = 'Mostoles'";
        Statement sentencia = conexio.createStatement();
        sentencia.executeUpdate(sql);
        sentencia.close();
        conexio.close();
    }
}
